const app = require('connect');

const env = process.env.NODE_ENV || 'development';

const setup = (format) => {
    const regexp = /:(\w+)/g;

    //throw new Error('error in hello');

    return (req, res, next) => {
        const str = format.replace(regexp, (match, property) => {
            console.log(" Match: " + match);
            console.log(property);
            return req[property];
        })
        console.log(str);
        next();
    }
}

const hello = (req, res, next) => {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World');
}

const errorHandler = (err, req, res, next) => {
    res.statusCode = 500;
    switch(env) {
        case 'development':
            console.log('Error: ' + err);
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(err));
        break;
        case 'production':
            res.send('Server Error');
        break;
    }
}

app().use(setup(':method :url'))
    .use((req, res) => {
        foo();
        res.setHeader('Content-Type', 'text/plain');
        res.end('hello world');
    })
    .use(hello)
    .use(errorHandler)
    .listen(3000);

