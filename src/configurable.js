const app = require('connect');

const setup = (format) => {
    const regexp = /:(\w+)/g;
    
    return (req, res, next) => {
        const str = format.replace(regexp, (match, property) => {
            console.log(" Match: " + match);
            console.log(property);
            return req[property];
        })
        console.log(str);
        next();
    }
}

const hello = (req, res) => {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World');
}

app().use(setup(':method :url'))
    .use(hello)
    .listen(3000);

